# Running Gitlab EE Locally
Using [Docker Compose](https://docs.docker.com/compose/), this project will install and configure an instance of GitLab EE. The installation of Gitlab EE will include a configured Gitlab Runner and an external Registry for container images.

## Prerequisites

1. If required, Gitlab EE License file
1. Install [Docker Desktop](https://www.docker.com/products/docker-desktop)
1. Recommended: Increase resources for Docker for Desktop (Preferences -> Advanced). Increase CPU and Memory at least one additional CPU and 1G. 

## Running Gitlab
1. Clone repository
    1. `$ git clone https://gitlab.com/joshswann/docker`
    1. This will clone the project into the default directory named `docker`. This directory `Docker`, will be used by Docker Compose.
1. In the root of the repository run the Docker Compose command
    1. `$ docker-compose up -d`
    1. This command will execute the `docker-compose.yml`. Pulling down the specified containers, configuring and starting the application. By using `-d` this will run in the background as a detached process. 
    1. If you'd like to view the startup logs, you can tail the logs `$ docker-compose logs -f`
1. After several minutes, open a browser to [http://docker.for.mac.localhost](http://docker.for.mac.localhost)
1. You will be prompted to set the `root` password of the admin user.
1. You're now directed to login. Use the username: `root` and the password specified on the previous step.
1. Gitlab is current running and you are now able to create projects and add users as needed.

## Adding Gitlab Runners
### Find Registration Token
1. Log into Gitlab UI
1. Go to the Admin Screen (click the wrench in the header)
1. Go to the Runners page (Overview -> Runners)
1. Under the `Set up a Runner manually` find the registration token

### Register Runner with Gitlab
1. Go back to the command prompt where you cloned the repository
1. Using the token from the administration screen g
1. Set `PROJECT_REGISTRATION_TOKEN` token: `$ export PROJECT_REGISTRATION_TOKEN=<TOKEN_VALUE>`
1. Verify the name of your docker runner: `$ docker ps`. You should see a running container named something like `docker_runner_1`. Use name of your `_runner_1` container in the following command.
1. Register the runner with Gitlab using the following command. Note: be sure to update the `docker_runner_1` container name if your container is different.

```
docker exec -d docker_runner_1 gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image "docker:19.03.1" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
  --docker-volume-driver overlay2 \
  --url "http://docker.for.mac.localhost" \
  --clone-url "http://docker.for.mac.localhost" \
  --registration-token $PROJECT_REGISTRATION_TOKEN \
  --description "docker-runner" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false" \
  --description "Amazing Docker Runner" \
  --access-level="not_protected"
```
5. Refresh the Runners screen. You should now see an available runner.

## Stopping / Removing Gitlab EE
### Stop
1. To stop or shutdown GitLab, from within the repository directory execute: `$ docker-compose stop`
1. This will stop the containers and maintain state.
1. To resume or start again, run `$ docker-compose start`
### Removing
1. To stop and remove Gitlab, from within the repository directory execute: `$ docker-compose down -v`
1. This will stop Gitlab and remove the containers and the associated volume data.

## Data Retention (Volumes)
This compose file specifies several volumes for the containers to use. Volumes aa mechanism for persisting data outside of the container. [Docker Volumes](https://docs.docker.com/storage/volumes/)

This project will persist all GitLab EE and GitLab Runner data into a directory named `srv` found within the same directory that the `docker-compose.yml` file located.

## Todos
* The Container Registry is external to the project because I didn't generate certificates to trust.
* Use Registry built into Gitlab EE, create certificate trust, etc. 

